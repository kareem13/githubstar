package com.githubstar.githubstarapp.repository

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.githubstar.githubstarapp.R
import com.githubstar.githubstarapp.model.Item

class RepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val image = view.findViewById<ImageView>(R.id.avatar)
    private val stars = view.findViewById<TextView>(R.id.stars)
    private val repoName = view.findViewById<TextView>(R.id.repositoryName)
    private val description = view.findViewById<TextView>(R.id.description)
    private val owner = view.findViewById<TextView>(R.id.owner)

    fun bindTo(item: Item) {
        stars.text = item.stargazersCount.toString()
        repoName.text = item.name
        description.text = item.description
        owner.text = item.owner.login
        Glide.with(itemView.context).load(item.owner.avatarUrl).into(image)
    }
}
