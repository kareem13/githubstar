package com.githubstar.githubstarapp.repository

import android.app.Application
import android.text.format.DateFormat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.githubstar.githubstarapp.model.Item
import com.githubstar.githubstarapp.network.GithubService
import com.githubstar.githubstarapp.utils.NetworkState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class RepositoryViewModel(
        application: Application
) : AndroidViewModel(application) {

    val status: MutableLiveData<NetworkState> = MutableLiveData()
    private val data: MutableLiveData<List<Item>> = MutableLiveData()

    fun getRepositories(): MutableLiveData<List<Item>> {
        loadRepositories()
        return data
    }

    private var disposable: Disposable? = null

    /**
     * Return (current date - 30days) as String
     * e.g. 2018/09/05 => 2018/08/04
     */
    private fun getPreviousMonthDate(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -1)
        return DateFormat.format("yyyy-MM-dd", calendar.time).toString()
    }

    private fun loadRepositories() {
        status.value = NetworkState.LOADING
        disposable = GithubService.create(getApplication()).getRepositories(
                date = "created:>${getPreviousMonthDate()}",
                page = 1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            status.value = NetworkState.LOADED
                            data.value = it.items
                        },
                        {
                            status.value = NetworkState.error(it.message)
                        }
                )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}
