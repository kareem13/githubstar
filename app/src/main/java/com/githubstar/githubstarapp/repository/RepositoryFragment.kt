package com.githubstar.githubstarapp.repository

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.githubstar.githubstarapp.R
import com.githubstar.githubstarapp.utils.Status
import com.githubstar.githubstarapp.utils.isOnline
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.repository_fragment.*


class RepositoryFragment : Fragment() {

    companion object {
        fun newInstance() = RepositoryFragment()
    }

    private lateinit var viewModel: RepositoryViewModel
    private lateinit var adapter: RepositoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.repository_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RepositoryViewModel::class.java)

        setupRecyclerView()

        getRepositories()

        viewModel.status.observe(this, Observer { network ->
            when (network.status) {
                Status.LOADING -> progressBar.visibility = View.VISIBLE
                Status.FAILED -> {
                    progressBar.visibility = View.INVISIBLE
                    if (!context?.isOnline()!!) {
                        Snackbar.make(repository, R.string.no_internet, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.try_again) {
                                    getRepositories()
                                }.show()
                    }
                }
                Status.SUCCESS -> progressBar.visibility = View.INVISIBLE
            }
        })
    }

    private fun getRepositories() {
        progressBar.visibility = View.VISIBLE
        viewModel.getRepositories().observe(this, Observer {
            adapter.setRepositoryList(it)
        })
    }

    private fun setupRecyclerView() {
        adapter = RepositoryAdapter()
        repoList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        repoList.adapter = adapter
    }
}

