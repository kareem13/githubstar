package com.githubstar.githubstarapp.repository

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.githubstar.githubstarapp.R
import com.githubstar.githubstarapp.model.Item

class RepositoryAdapter(
        private var data: List<Item> = arrayListOf()
) : RecyclerView.Adapter<RepositoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_repository, parent, false)
        return RepositoryViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        val repository = data[position]
        holder.bindTo(repository)
    }

    fun setRepositoryList(data: List<Item>) {
        this.data = data
        notifyDataSetChanged()
    }
}