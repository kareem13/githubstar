package com.githubstar.githubstarapp.utils

import android.content.Context
import android.net.ConnectivityManager

/**
check if phone connected to internet or offline
 */
fun Context.isOnline(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = cm.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

