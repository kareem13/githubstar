package com.githubstar.githubstarapp.utils

class Constants {
    companion object {
        const val BASE_URL = "https://api.github.com/"
    }
}