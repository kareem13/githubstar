package com.githubstar.githubstarapp.utils

enum class Status {
    LOADING,
    FAILED,
    SUCCESS
}

/**
 * return status of fetching data from server (loading, sucess or failed)
 */
data class NetworkState(
        val status: Status,
        val msg: String? = null) {
    companion object {
        val LOADED = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.LOADING)
        fun error(msg: String?) = NetworkState(Status.FAILED, msg)
    }
}