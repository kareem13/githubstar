package com.githubstar.githubstarapp.model

import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("total_count")
        val totalCount: Int = 0,
        @SerializedName("incomplete_results")
        val incompleteResults: Boolean = false,
        val items: List<Item> = listOf()
)