package com.githubstar.githubstarapp.model

import com.google.gson.annotations.SerializedName

data class Item(
        val name: String = "",
        val owner: Owner = Owner(),
        val description: String = "",
        @SerializedName("stargazers_count")
        val stargazersCount: Int = 0
)