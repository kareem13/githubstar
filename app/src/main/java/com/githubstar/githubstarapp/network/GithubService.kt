package com.githubstar.githubstarapp.network

import android.content.Context
import com.githubstar.githubstarapp.model.Response
import com.githubstar.githubstarapp.utils.Constants
import com.githubstar.githubstarapp.utils.isOnline
import io.reactivex.Observable
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.File
import java.util.concurrent.TimeUnit

interface GithubService {
    @GET("/search/repositories")
    fun getRepositories(
            @Query("q", encoded = true) date: String,
            @Query("order") order: String = "desc",
            @Query("sort") sort: String = "stars",
            @Query("page") page: Long
    ): Observable<Response>

    companion object {
        /**
         * create retrofit instance to contact github rest api
         */
        fun create(context: Context): GithubService {
            // Create ok http client instance
            val httpClient = OkHttpClient.Builder()
                    .cache(getCache(context))
                    .addInterceptor(getLoggingInterceptor())
                    .addInterceptor(getOfflineCacheInterceptor(context))
                    .addNetworkInterceptor(getNetworkTemporaryCache())
                    .build()

            // Create retrofit instance
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .baseUrl(Constants.BASE_URL)
                    .build()

            return retrofit.create(GithubService::class.java)
        }

        /**
         * cache setup
         */

        private fun getCache(context: Context): Cache {
            val cacheSize = 10 * 1024 * 1024 // 10 MB
            val httpCacheDirectory = File(context.cacheDir, "responses")
            return Cache(httpCacheDirectory, cacheSize.toLong())
        }

        /**
         * create logging interceptor to check requests
         */
        private fun getLoggingInterceptor(): HttpLoggingInterceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return loggingInterceptor
        }

        /**
         * create offline backup of all responses for 7 days
         */

        private fun getOfflineCacheInterceptor(context: Context): Interceptor {
            return Interceptor {
                var request = it.request()
                if (!context.isOnline()) {
                    val cacheControl = CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build()

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build()
                }
                return@Interceptor it.proceed(request)

            }
        }

        /**
         * cache response from server for 2min before fetching again
         */
        private fun getNetworkTemporaryCache(): Interceptor {
            return Interceptor {
                val response = it.proceed(it.request())
                val cacheControl = CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build()

                return@Interceptor response.newBuilder()
                        .header("cache-control", cacheControl.toString())
                        .build()
            }
        }
    }
}